import Exponent from 'exponent';
import React from 'react';
import {
  AppRegistry,
  Platform,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import {
  NavigationProvider,
  StackNavigation,
} from '@expo/ex-navigation';
import {
  FontAwesome,
} from '@expo/vector-icons';
import { createStore, applyMiddleware, combineReducers, bindActionCreators } from 'redux';
import { Provider, connect } from 'react-redux';
import thunk from 'redux-thunk';

import Router from './navigation/Router';
import cacheAssetsAsync from './utilities/cacheAssetsAsync';

import { load } from './actions/main.js'

class AppContainer extends React.Component {

  static propTypes = {
        loaded: React.PropTypes.bool.isRequired,
        load: React.PropTypes.func.isRequired,
    };
/*
  componentDidMount() {
        setTimeout(() => {
            this.props.load()
        }, 5000)
    }
*/

  componentWillMount() {
    this._loadAssetsAsync();
  }

  async _loadAssetsAsync() {
    try {
      await cacheAssetsAsync({
        images: [
          require('./assets/images/exponent-wordmark.png'),
        ],
        fonts: [
          FontAwesome.font,
          {'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf')},
        ],
      });
    } catch(e) {
      console.warn(
        'There was an error caching assets (see: main.js), perhaps due to a ' +
        'network timeout, so we skipped caching. Reload the app to try again.'
      );
      console.log(e.message);
    } finally {
      this.props.load();
    }
  }

  render() {
    if (this.props.loaded) {
      return (
          <View style={styles.container}>
            <NavigationProvider router={Router}>
              <StackNavigation id="root" initialRoute={Router.getRoute('rootNavigation')} />
            </NavigationProvider>

            {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
            {Platform.OS === 'android' && <View style={styles.statusBarUnderlay} />}
          </View>
      );
    } else {
      return (
          <Exponent.Components.AppLoading />
      );
    }
  }
} 

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  statusBarUnderlay: {
    height: 24,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
});


const mapStateToProps = function(state) {
    console.log(state)
    return {
        loaded: state.main.loaded,
    };
};

const mapDispatchToProps = function(dispatch) {
    return bindActionCreators({
      load
    }, dispatch)
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppContainer);