import Exponent, {Components} from 'exponent';
import React from 'react';

import App from './container.js'

Exponent.registerRootComponent(App);