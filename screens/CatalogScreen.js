import React from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text
} from 'react-native';

import {
  ExpoLinksView,
} from '@expo/samples';

import ItemList from '../screens/itemlist.js';

export default class CatalogScreen extends React.Component {
  static route = {
    navigationBar: {
      title: 'Catalog',
    },
  }

  render() {
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={this.props.route.getContentContainerStyle()}>

        { /* Go ahead and delete ExponentLinksView and replace it with your
           * content, we just wanted to provide you with some helpful links */ }
        <View>
            <Text>
                -- Catalog --        
            </Text>
            <Text>
                ** Hola  **
            </Text>
            <Text>
                == REDUX  ==
            </Text>
            
        </View>
        <ItemList />
      </ScrollView>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
  },
});
