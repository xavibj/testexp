import React, { Component } from 'react';

import {
  ScrollView,
  StyleSheet,
  View,
  Text
} from 'react-native';

class ItemList extends Component {
    constructor(){
        super();

        this.state={
            items:[
                {
                    id: 1,
                    label: 'List item 1'
                },
                 {
                    id: 2,
                    label: 'List item 2'
                },
                 {
                    id: 3,
                    label: 'List item 3'
                },
                 {
                    id: 4,
                    label: 'List item 4'
                },
            ],
            hasErrored: false,
            isLoading: false
        }
    }

    render(){
        return <View><Text>Hola</Text></View>;
        /*
        if (this.state.hasErrored) {
            return <p> Sorry! there was an error loading items </p>
        }

        if (this.state.isLoading){
            return <p>Loading...</p>
        }

        return(
            <ul>
                {this.state.items.map((item) => (
                    <li key={item.id}>
                        {item.label}
                    </li>
                ))}
            </ul>
        )
        */
    }
}

export default ItemList;