export const CATALOG_STARTED = 'CATALOG_STARTED'
export const CATALOG_RESULT = 'CATALOG_RESULT'
export const CATALOG_FAILED = 'CATALOG_FAILED'
export const MORE_CATALOG = 'MORE_CATALOG'

const _getCatalog = (dispatch, nextPage=0) => {
    dispatch(_catalogStarted())
    var url = 'https://locodeofertas.com/wp-json/wc/v1/products?offset='+nextPage;

    fetch(url, {headers: {
        'Authorization': 'Basic ' + base64.encode('ck_c559be5474f70709debe3dbd7ea2cfafbb4b3e2b' + ":" + 'cs_dafda2cf948842f9d6dace5dc45221964ddf7383')
    }})
      .then((response) => response.json())
      .then((responseData) => {
          if (responseData.error) throw responseData.error.message
          return responseData
      })
      .then((data) => {
          if (nextPage > 0) {
              dispatch(_moreCatalogReceived(data))
          } else {
              dispatch(_catalogReceived(data))
          }
      })
      .catch((err) => {
          dispatch(_catalogFailed(err))
      });
}

const _catalogStarted = () => ({type: CATALOG_STARTED})
const _moreCatalogReceived = (data) => ({type: MORE_CATALOG, data})
const _catalogReceived = (data) => ({type: CATALOG_RESULT, data})
const _catalogFailed = (message) => ({type: CATALOG_FAILED, message})