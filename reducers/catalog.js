import {
    CATALOG_STARTED,
    CATALOG_RESULT, 
    CATALOG_FAILED, 
    MORE_CATALOG
} from '../actions/catalog'

const initiaCatalogState = {
    isLoading: false
}

export const catalog = (state = initialCatalogState, action) => {
    switch (action.type) {
        case CATALOG_STARTED:
            return Object.assign({}, state, {isLoading: true})
        case CATALOG_FAILED:
            return Object.assign({}, state, {
                
            })
        default:
            return state
    }
} 