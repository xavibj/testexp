import {
    LOADING
} from '../actions/main.js'

const initialMainState = {
    loaded: false
}

export const main = (state = initialMainState, action) => {
    switch (action.type) {
        case LOADING:
            return Object.assign({}, state, {loaded: true})
        default:
            return state
    }
} 